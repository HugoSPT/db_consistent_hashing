# external libraries
import logging
import md5
from sqlite3 import \
	connect, \
	OperationalError

# own libraries
from errors import NoDataFound

NUM_TABLES = 4
TABLE_NAME = 'comment'

def logger_init():
    global logger
    
    logging.basicConfig(level=logging.INFO,
                format='[%(asctime)s.%(msecs)d] %(levelname)s : %(message)s', 
                datefmt="%Y-%m-%d %H:%M:%S"
            )

class DB:
	def __init__(self, db_name, num_tables=None):
		global NUM_TABLES

		self.db_name = db_name
		self.conn = None
		self.cursor = None
		if num_tables:
			NUM_TABLES = num_tables

		self.tables_keys = {}

		self.connect()
		self._gen_table_keys()
		logger_init()

	def connect(self):
		"""
			Connects with the db
		"""

		self.conn = connect(self.db_name)
		self.cursor = self.conn.cursor()

	def commit(self):
		"""
			Commits all changes since the last commit
		"""

		self.conn.commit()

	def close(self):
		"""
			Close the connection with the db
		"""

		self.cursor.close()

	def execute(self, query):
		"""
			Executes a given query

			Args:
				query: the query to be executed

			Returns:
				result: the resulted comment
				None: when there is no comment for obj_id
		"""

		logging.info(
			"Will execute query {query}".format(
				query=query
			)
		)
		
		try:
			self.cursor.execute(query)
		except OperationalError, e:
			logging.error("Something went wrong executing the query (doest the table exist?):{error}".format(error=e))
			return

		result = self.cursor.fetchall()
		if len(result) > 0:
			msg = ''
			if len(result) < 10:
				msg = "Query result: {result}".format(result=result)
			else:
				msg = "Query result too long to be printed ({size}).".format(size=len(result))
			
			logging.info(msg)

			return result
		else:
			raise NoDataFound

	def delete_comment(self, obj_id, table=None):
		"""
			Deletes a comment from the db

			Args:
				obj_id: the comment id to be deleted
				table: the table where the comment is
		"""

		if table:
			logging.info(
				"Will delete comment {obj_id} in table {table}".format(
					obj_id=obj_id,
					table=table
				)
			)
			self.cursor.execute(
				"DELETE FROM {table} WHERE id={obj_id}".format(
					table = table,
					obj_id=int(obj_id)
				)
			)
		else:
			table, _ = _get_table(obj_id)
			logging.info(
				"Will delete comment {obj_id} in table {table}".format(
					obj_id=obj_id,
					table=table
				)
			)
			self.cursor.execute(
				"DELETE FROM {table} WHERE id={obj_id}".format(
					table = table,
					obj_id=int(obj_id)
				)
			)

		logging.info("Comment deleted.")

	def save_comment(self, obj_id, comment, table=None):
		"""
			Save a comment into the db

			Args:
				obj_id: the comment id
				comment: the comment text
		"""

		if not table:
			table, _ = self._get_table(obj_id)

		logging.info("Inserting comment {obj_id} in {table}".format(obj_id=obj_id, table=table))

		# Instead of using replace we can do a query and update if this obj_id already exists
		# or insert if not
		self.cursor.execute(
			"INSERT OR REPLACE INTO {table}(id, comment) values({obj_id},'{comment}');".format(
				table=table,
				obj_id=int(obj_id),
				comment=comment
			)
		
		)
		self.commit()
		logging.info("Comment inserted.")

	def get_comment(self, obj_id):
		"""
			Get a comment from the db

			Args:
				obj_id: the comment id

			Returns:
				result: the comment in the form of (obj_id, comment)
		"""

		table, key = self._get_table(obj_id)

		logging.info("Getting comment {obj_id} from {table}".format(obj_id=obj_id, table=table))

		result = self.execute(
					"SELECT * FROM {table} WHERE id={id};".format(
							table=table,
							id=obj_id
						)
					)
		if result:
			return result[0]

		logging.warning("Comment {obj_id} not found in {table}. Searching in other tables.".format(
								obj_id=obj_id,
								table=table
							)
						)

		# If this entry is not in table (because the number of tables changed)
		# we need to get it from other table if it exists and store in the new one
		sorted_keys = sorted(self.tables_keys)
		pos = sorted_keys.index(key)

		# We only need to search in tables with hash bigger than the last one
		for key in sorted_keys[pos:]:
			logging.warning("Searching {obj_id} in {table}.".format(
								obj_id=obj_id,
								table=self.tables_keys[key]
							)
						)
			result = self.execute(
						"SELECT * FROM {table} WHERE id={id};".format(
								table=self.tables_keys[key], 
								id=obj_id
							)
						)

			if result:
				# Now that we found this entry on the correct table,
				# lets remove the older one
				logging.warning("Comment {obj_id} found. Searching in other tables.".format(obj_id=obj_id))

				self.delete_comment(obj_id, table)
				self.save_comment(obj_id, result[0][1])
				return result

		return None

	def update_tables(self):
		"""
			Updates all tables deleting/inserting rows from/into the correct table
		"""

		for key, table in self.tables_keys.items():
			
			try:
				results = self.execute("SELECT * FROM {table}".format(table=table))
				for obj_id, comment in results:
					new_table, _ = self._get_table(obj_id)
					if new_table != table:
						logging.info(
							"Moving comment {obj_id} from {table} to {new_table}".format(
								obj_id=obj_id,
								table=table,
								new_table=new_table
							)
						)
						self.delete_comment(obj_id, table)
						self.save_comment(obj_id, comment, new_table)

			except NoDataFound, e:
				pass

	def _gen_table_keys(self):
		"""
			Generates the keys (intervals) for every table
		"""

		for table_num in xrange(0, NUM_TABLES):
			table_name = TABLE_NAME + str(table_num)
			self.tables_keys[self._gen_hash(table_name)] = table_name

	def _get_table(self, obj_id):
		"""
			Gets the table name where a comment can be stored

			Args:
				obj_id: the comment id

			Returns:
				table: the table name where the comment should be stored
				key: the key associated with the table
		"""

		obj_hash = self._gen_hash(obj_id)

		sorted_keys = sorted(self.tables_keys)

		for key in sorted_keys:
			if obj_hash <= key:
				return self.tables_keys[key], key

		return self.tables_keys[sorted_keys[0]], key

	def _gen_hash(self, obj_id):
		"""
			Generates a MD5 hash for a given comment id

			Args:
				obj_id: the comment id
				
			Returns:
				hexdigest: returns a 16-bit long number built from a 32-bit hex string
		"""

		m = md5.new()
		m.update(str(obj_id))
		return long(m.hexdigest(), 16)
