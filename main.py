import argparse
import logging

from comment import Comment
import db
from errors import NoDataFound

def logger_init():
    logging.basicConfig(level=logging.INFO,
                format='[%(asctime)s.%(msecs)d] %(levelname)s : %(message)s', 
                datefmt="%Y-%m-%d %H:%M:%S"
            )

def insert_comment(obj_id, comment):
	"""
		Insert a comment provided from command line

		Args:
			obj_id: the comment id
			comment: the command text
	"""

	comment = Comment(obj_id, comment)
	commet.save()

def get_comment(obj_id):
	"""
		Get a comment from the db

		Args:
			obj_id: the comment id
	"""

	comment = Comment.get_object_by_id(obj_id)
	logging.info("Result: obj_id {obj_id}, comment {comment}".format(
					obj_id=comment.id,
					comment=comment.comment
				)
			)

	return comment

def get_comments(table):
	"""
		Get all comments in a given table

		Args:
			table: the table to be searched
	"""

	try:
		db_conn = db.DB('ingresso.sqlite')
		results = db_conn.execute("SELECT * FROM {table};".format(table=table))
		db_conn.close()
		logging.info("Entries in {table}: {results}".format(table=table, results=results))
		return results
	except NoDataFound, e:
		logging.error("No data found in {table}.".format(table=table))

def update_tables(num_tables):
	db_conn = db.DB('ingresso.sqlite', int(num_tables))
	db_conn.update_tables()
	db_conn.close()

if __name__ == '__main__':

	logger_init()
    
	parser = argparse.ArgumentParser(description='Search and insert comments')

	parser.add_argument('--id', dest='id', help='The comment id. If this comment id \
												already exist, it will update the comment.')
	parser.add_argument('--comment', dest='comment', help='The comment text to be inserted (should be used with --id).')
	parser.add_argument('--table', dest='table', help='Get all the comments from table')
	parser.add_argument('--update_tables', dest='tables', help='Give the new number of tables')

	args = parser.parse_args()

	if args.id and args.comment:
		insert_comment(args.id, args.comment)
	elif args.id:
		get_comment(args.id)
	elif args.table:
		get_comments(args.table)
	elif args.tables:
		update_tables(args.tables)
