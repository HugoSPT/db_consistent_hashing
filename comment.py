import logging

import db
from errors import NoDataFound

class Comment:
    def __init__(self, obj_id, comment=None):
        self.id = obj_id
        self.comment = comment
    
    def save(self):
        """
            Save this comment into the db
        """

        db_conn = db.DB('ingresso.sqlite')
        db_conn.save_comment(self.id, self.comment)
        db_conn.close()
    
    @classmethod   
    def get_object_by_id(self, obj_id):
        """
            Gets the comment with a given id

            Args:
                obj_id: the comment id to be searched
        """

        db_conn = db.DB('ingresso.sqlite')
        try:
            comment = db_conn.get_comment(obj_id)
            return Comment(comment[0], comment[1])
        except NoDataFound, e:
            logging.error("No data found for {obj_id}".format(obj_id=obj_id))
            return None
