# external libraries
import logging
import sqlite3
import sys
from random import randint

from comment import Comment
import db
from errors import NoDataFound

# This NUM_REPLICAS should have the same value as in db
NUM_TABLES = 4
DB_NAME = 'ingresso.sqlite'
TABLE_NAME = 'comment'

def logger_init():
    global logger
    
    logging.basicConfig(level=logging.INFO,
                format='[%(asctime)s.%(msecs)d] %(levelname)s : %(message)s', 
                datefmt="%Y-%m-%d %H:%M:%S"
            )

if __name__ == '__main__':

    logger_init()
    
    if len(sys.argv) > 0:
        NUM_TABLES = int(sys.argv[1])

    db_conn = db.DB(DB_NAME, NUM_TABLES)
    
    logging.info("Table creation starting: {num} tables".format(num=NUM_TABLES))
    for i in range(0, NUM_TABLES):
        try:
            result = db_conn.execute("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='{table_name}';".format(table_name=TABLE_NAME + str(i)))

            if result[0][0] == 0:
                logging.info("Will create table {table_name}".format(table_name=TABLE_NAME + str(i)))
                db_conn.execute("CREATE TABLE '{table_name}'(id INTEGER PRIMARY KEY, comment TEXT NOT NULL);".format(table_name=TABLE_NAME + str(i)))

            else:
                logging.info("Table {table_name} already exists".format(table_name=TABLE_NAME + str(i)))
        except NoDataFound:
            pass

    db_conn.commit()
    db_conn.close()
    
    logging.info("Will generate 10000 comments to be inserted into the db.")

    i = 0
    while i < 10000:
        comment = Comment(i, "Comment " + str(randint(0, 999999)))
        comment.save()
        i = i + 1
    logging.info("Comments generated and inserted.")
